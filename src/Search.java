/**
 * Created by Dima on 24.02.2017.
 */
public class Search {

    public static final String STRING = "When you walk through a storm, hold your head up high" +
            "And don't be afraid of the dark" +
            "At the end of the storm, there's a golden sky" +
            "And the sweet, silver song of a lark" +
            "Walk on through the wind" +
            "Walk on through the rain" +
            "Though your dreams be tossed and blown" +
            "Walk on, walk on" +
            "With hope in your hearts" +
            "And you'll never walk alone" +
            "You'll never walk alone" +
            "Walk on, walk on" +
            "With hope in your hearts" +
            "And you'll never walk alone" +
            "You'll never walk alone";
    public static final String SUBSTRING = "You'll never walk alone";

    public static void main(String[] args) {
        String string = STRING;
        String subString = SUBSTRING;

        System.out.println(string);
        System.out.println(subString);


        System.out.println(find(string, subString));
        System.out.println(string.indexOf(subString));
    }

    private static int find(String string, String subString) {
        int i = 0;
        while (i < string.length()) {
            int progress = 0;
            int current = i;
            while (progress < subString.length()) {
                if (subString.charAt(progress) == string.charAt(current)) {
                    current++;
                    progress++;
                    if (progress==subString.length())return i;
                }else {
                    break;
                }
            }
            i++;
        }
        return -1;
    }
}
