import java.util.Random;

/**
 * Created by Dima on 24.02.2017.
 */
public class QuickSort {
    public static int ARRAY_LENGTH = 1000;
    private static int[] array = new int[ARRAY_LENGTH];
    private static Random generator = new Random();

    public static void initArray() {
        for (int i=0; i<ARRAY_LENGTH; i++) {
            array[i] = generator.nextInt(100);
        }
    }

    public static void printArray() {
        for (int i=0; i<ARRAY_LENGTH-1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println(array[ARRAY_LENGTH-1]);
    }

    public static void quickSort() {
        doSort(0, ARRAY_LENGTH-1);
    }

    private static void doSort(int begin, int end) {
        if (begin >= end)
            return;
        int left = begin, right = end;

        int current = (left + right) / 2;

        while (left < right) {
            while (left < current && (array[left] <= array[current])) {
                left++;
            }
            while (right > current && (array[current] <= array[right])) {
                right--;
            }
            if (left < right) {
                int temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                if (left == current)
                    current = right;
                else if (right == current)
                    current = left;
            }
        }
        doSort(begin, current);
        doSort(current+1, end);
    }

    public static void main(String[] args) {
        initArray();
        printArray();
        quickSort();
        printArray();
    }
}